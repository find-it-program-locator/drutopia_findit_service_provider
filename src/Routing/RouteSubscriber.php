<?php

namespace Drupal\drutopia_findit_service_provider\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Always deny access to '/node/add' listing of available content to create.
    if ($route = $collection->get('node.add_page')) {
      // Note that the second parameter of setRequirement() is a string.
      $route->setRequirement('_permission', 'administer nodes');
    }
  }

}
